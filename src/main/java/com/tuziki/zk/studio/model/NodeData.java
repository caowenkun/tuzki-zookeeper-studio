package com.tuziki.zk.studio.model;

/**
 * Created by Sannychan on 2015/3/29.
 */
public class NodeData {

    private String nodeName;
    private String nodePath;
    private String parentPath;

    private boolean expand = false;  //是否已经获取过子节点数据

    public NodeData(String parentPath, String nodeName) {
        this.parentPath = parentPath;
        this.nodeName = nodeName;
        if (parentPath == null || parentPath.trim() == "") {
            this.nodePath = "/";
        } else if (parentPath == "/") {
            this.nodePath = "/" + nodeName;
        } else {
            this.nodePath = parentPath + "/" + nodeName;
        }


    }

    public String getNodeName() {
        return nodeName;
    }

    public String getNodePath() {
        return nodePath;
    }

    public String getParentPath() {
        return parentPath;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public void setNodePath(String nodePath) {
        this.nodePath = nodePath;
    }

    public void setParentPath(String parentPath) {
        this.parentPath = parentPath;
    }

    public boolean isExpand() {
        return expand;
    }

    public void setExpand(boolean expand) {
        this.expand = expand;
    }

    @Override
    public String toString() {
        return this.nodeName;
    }
}
