package com.tuziki.zk.studio.util;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

/**
 * Created by Sannychan on 2015/3/27.
 */
public class ZKPrintStream extends PrintStream {
    public ZKPrintStream() {
        super(new OutputStream() {//匿名OutputStream
            public void write(int b) throws IOException {
                //不用实现，只是为了比父类增加一个无参构造函数
            }
        });
    }
}
