package com.tuziki.zk.studio.util;

import com.tuziki.zk.studio.view.MainView;
import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.spi.LoggingEvent;
import org.apache.log4j.spi.ThrowableInformation;

/**
 * Created by Sannychan on 2015/3/23.
 */
public class LogAppender extends AppenderSkeleton {
    @Override
    protected void append(LoggingEvent loggingEvent) {
        String log = this.getLayout().format(loggingEvent);
        MainView.appendMsg(log);
        ThrowableInformation throwableInformation = loggingEvent.getThrowableInformation();
        if(throwableInformation != null){
            for(String msg:throwableInformation.getThrowableStrRep()){
                MainView.appendMsg(msg+"\n");
            }
        }
    }

    @Override
    public void close() {

    }

    @Override
    public boolean requiresLayout() {
        return true;
    }
}
