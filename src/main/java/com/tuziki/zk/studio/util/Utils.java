package com.tuziki.zk.studio.util;

import org.apache.log4j.Logger;
import org.jb2011.lnf.beautyeye.winlnfutils.WinUtils;

import javax.swing.*;
import javax.swing.plaf.FontUIResource;
import java.awt.*;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.Enumeration;

/**
 * Created by Sannychan on 2015/3/25.
 */
public class Utils {

    static Logger logger = Logger.getLogger(Utils.class.getName());

    public static void initSystemOut() throws UnsupportedEncodingException {
        System.setOut(PrintStreamProxy.getProxy());
        System.setErr(PrintStreamProxy.getProxy());
        System.out.println("日志系统初始化完成~");
    }

    public static Font getGlobalFont() {
        Font font = null;
        int fontSize = 12;
        if (WinUtils.isOnVista())
            font = new Font("微软雅黑", 0, fontSize);
        else {
            font = new Font("宋体", 0, fontSize);
        }
        return font;
    }

    public static void initGlobalFont() {
        Font font = getGlobalFont();
        FontUIResource fontRes = new FontUIResource(font);
        Enumeration keys = UIManager.getDefaults().keys();
        while (keys.hasMoreElements()) {
            Object key = keys.nextElement();
            Object value = UIManager.get(key);
            if (value instanceof FontUIResource) {
                UIManager.put(key, fontRes);
            }
            ;
        }
    }
}
