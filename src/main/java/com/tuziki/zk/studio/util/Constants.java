package com.tuziki.zk.studio.util;

import java.awt.*;

/**
 * Created by Sannychan on 2015/3/21.
 */
public class Constants {

    public static Dimension DEFAULT_DIMENSION = new Dimension(1024, 768);

    public static Dimension MIN_DIMENSION = new Dimension(600, 480);

    public static String CONNECT_HOISTORY_FILE = "zk-studio.history";

    public static String ZK_ROOT_PATH = "/";

    public static String[] DATA_TAB_NAMES = new String[]{"Data", "MetaData", "ACL"};


    public static final String A_VERSION = "ACL Version";
    public static final String C_TIME = "Creation Time";
    public static final String C_VERSION = "Children Version";
    public static final String CZXID = "Creation ID";
    public static final String DATA_LENGTH = "Data Length";
    public static final String EPHEMERAL_OWNER = "Ephemeral Owner";
    public static final String M_TIME = "Last Modified Time";
    public static final String MZXID = "Modified ID";
    public static final String NUM_CHILDREN = "Number of Children";
    public static final String PZXID = "Node ID";
    public static final String VERSION = "Data Version";
    public static final String ACL_PERMS = "Permissions";
    public static final String ACL_SCHEME = "Scheme";
    public static final String ACL_ID = "Id";
    public static final String SESSION_STATE = "Session State";
    public static final String SESSION_ID = "Session ID";
}
