package com.tuziki.zk.studio.view;

import com.tuziki.zk.studio.util.Constants;
import com.tuziki.zk.studio.util.Utils;
import org.apache.log4j.Logger;
import org.jb2011.lnf.beautyeye.BeautyEyeLNFHelper;

import javax.swing.*;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import java.awt.*;

/**
 * Created by Sannychan on 2015/3/21.
 */
public class Bootstrap {


    static Logger logger = Logger.getLogger(Bootstrap.class.getName());

    private static JFrame frame = null;

    private static JFrame connectView = null;

    public static void main(String[] args) throws Exception {
        Utils.initSystemOut();
        BeautyEyeLNFHelper.debug = true;
        BeautyEyeLNFHelper.launchBeautyEyeLNF();
        UIManager.put("RootPane.setupButtonVisible", false);
        Utils.initGlobalFont();
        new Bootstrap(GraphicsEnvironment.getLocalGraphicsEnvironment()
                .getDefaultScreenDevice().getDefaultConfiguration());

    }

    public Bootstrap(GraphicsConfiguration gc) {
        if (frame == null) {
            frame = createFrame(gc);
        }
        initMenu();
        initContent();
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                frame.setVisible(true);
            }
        });
    }

    private void initMenu() {
        frame.setJMenuBar(createMenus());
    }

    private void initContent() {
        frame.getContentPane().add(MainView.initContentGroupLayout(), BorderLayout.CENTER);
    }


    public static JFrame createFrame(GraphicsConfiguration gc) {
        frame = new JFrame(gc);
        frame.setSize(Constants.DEFAULT_DIMENSION);
        frame.setMinimumSize(Constants.MIN_DIMENSION);
        frame.setPreferredSize(Toolkit.getDefaultToolkit().getScreenSize());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout());
        JPanel contentPanel = new JPanel();
        contentPanel.setLayout(new BorderLayout());
        frame.setContentPane(contentPanel);
        return frame;
    }


    public JMenuBar createMenus() {
        JMenuBar menuBar = new JMenuBar();
        JMenu connectMenu = menuBar.add(new JMenu("连接"));
        connectMenu.addMenuListener(new MenuListener() {
            @Override
            public void menuSelected(MenuEvent e) {
                java.awt.EventQueue.invokeLater(new Runnable() {
                    public void run() {
                        showConnectView();
                    }
                });
            }

            @Override
            public void menuDeselected(MenuEvent e) {

            }

            @Override
            public void menuCanceled(MenuEvent e) {

            }
        });
        return menuBar;
    }


    private void showConnectView() {
        if (connectView != null) {
            connectView.setVisible(true);
            connectView.requestFocus();
            return;
        }
        connectView = new ConnectView();
        Dimension mainDimension = frame.getSize();
        Dimension connectDimension = connectView.getSize();
        int left = (int) (mainDimension.getWidth() - connectDimension.getWidth()) / 2;
        left = left > 0 ? left : 0;
        int top = (int) (mainDimension.getHeight() - connectDimension.getHeight()) / 2;
        top = top > 0 ? top : 0;
        connectView.setLocation(left, top);
        connectView.setVisible(true);
        connectView.requestFocus();
    }

}
