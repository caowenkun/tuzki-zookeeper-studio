package com.tuziki.zk.studio.view;

import com.tuziki.zk.studio.util.Constants;
import com.tuziki.zk.studio.util.Utils;
import org.apache.log4j.Logger;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Sannychan on 2015/3/21.
 */
public class MainView {

    static Logger logger = Logger.getLogger(MainView.class.getName());

    private static JPanel content = new JPanel();


    private static JScrollPane treePane = null;

    private static JPanel dataPane = new JPanel();

    private static JScrollPane logPane = null;

    private static JTextArea logTextArea = new JTextArea();

    private static JTabbedPane dataTabPane = new JTabbedPane();

    private static Map<String, JTextPane> tabsPanel = new HashMap<String, JTextPane>();


    public static JPanel initContentGroupLayout() {
        logger.info("init content group layout");

        initTreePane();

        initDataPane();

        initLogPane();

        GroupLayout contentLayout = new GroupLayout(content);
        content.setLayout(contentLayout);
        contentLayout.setHorizontalGroup(
                contentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(contentLayout.createSequentialGroup()
                                .addComponent(treePane, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(contentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(dataPane, javax.swing.GroupLayout.DEFAULT_SIZE, 507, Short.MAX_VALUE)
                                        .addComponent(logPane, javax.swing.GroupLayout.DEFAULT_SIZE, 507, Short.MAX_VALUE)))
        );
        contentLayout.setVerticalGroup(
                contentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(treePane, javax.swing.GroupLayout.DEFAULT_SIZE, 226, Short.MAX_VALUE)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, contentLayout.createSequentialGroup()
                                .addComponent(dataPane, javax.swing.GroupLayout.DEFAULT_SIZE, 276, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(logPane, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        return content;
    }


    private static void initDataPane() {
        dataPane = new JPanel();
        dataPane.setLayout(new BorderLayout());
        //dataPane.setBackground(new Color(150, 151, 111));
        dataPane.add(initDataTabPane());
        logger.info("dataPane init success");
    }

    private static JTabbedPane initDataTabPane() {
        tabsPanel = new HashMap<String, JTextPane>();
        dataTabPane = new JTabbedPane();
        for (String tab : Constants.DATA_TAB_NAMES) {
            JScrollPane jScrollPane = new JScrollPane();
            JTextPane jTextPane = new JTextPane();
            jScrollPane.setViewportView(jTextPane);
            dataTabPane.addTab(tab, jScrollPane);
            tabsPanel.put(tab, jTextPane);
        }
        return dataTabPane;
    }

    private static void initLogPane() {
        logPane = new JScrollPane();
        logTextArea.setColumns(20);
        logTextArea.setRows(5);
        logTextArea.setFont(Utils.getGlobalFont());
        logTextArea.setEditable(false);
        logPane.setViewportView(logTextArea);
        logger.info("logPane init success");
    }

    private static void initTreePane() {
//        DefaultMutableTreeNode root = new DefaultMutableTreeNode("root");
//        for (int i = 0; i < 50; i++) {
//            DefaultMutableTreeNode child = new DefaultMutableTreeNode("child_" + i);
//            root.add(child);
//        }
        JTree tree = new JTree() {
            public Insets getInsets() {
                return new Insets(5, 5, 5, 5);
            }
        };
        tree.setBackground(new Color(11, 250, 250));
        treePane = new JScrollPane();
        logger.info("treePane init success");
    }


    public static JScrollPane getTreePane() {
        return treePane;
    }

    public static void appendMsg(String msg) {
        logTextArea.append(msg);
    }

    public static Map<String, JTextPane> getTabsPanel() {
        return tabsPanel;
    }
}
