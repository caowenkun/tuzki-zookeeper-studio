#tuzki-zookeeper-studio

开源的zookeeper可视化客户端，主要用于zookeeper节点数据的查看。


使用Apache Curator 完成与zookeeper的链接；

使用[**BeautyEye on GitHub**](https://github.com/JackJiang2011/beautyeye)完成swing界面的开发；



> 本工具使用BeautyEye3.6版本。


#添加jar包到本地Maven仓库

使用如下命令：

`mvn install:install-file -Dfile=E:\maven-reop-local\beautyeye_lnf.jar -DgroupId=beautyeye_lnf -DartifactId=beautyeye_lnf -Dversion=3.6 -Dpackaging=jar`

>注意修改本地jar包路径

#项目截图
![截图一](http://git.oschina.net/uploads/images/2015/0402/220942_04b31847_312956.jpeg)
![截图二](http://git.oschina.net/uploads/images/2015/0402/220951_2eaee57d_312956.jpeg)
![截图三](http://git.oschina.net/uploads/images/2015/0402/220959_f8a9463e_312956.jpeg)